/**
 * Created by tomek on 09.01.16.
 */

Meteor.methods({
    updateUserData : function(userId, values){

        Meteor.users.update(Meteor.userId(), {$set :{ nick: values['nick'], username: values['email'], position: values['position'], address :  values['address'], skills: values['education']}});
    },
   removeDefinedUser : function(projectId,foruser)
   {
       Project.update(projectId, {$pull:{forUsers: foruser}})
   },
    addUserToProject : function(projectId, foruser){
        Project.update(projectId, {$push:{forUsers: foruser}})
    },
    updateTask : function(taskId, values){


        if(Tasks.find({_id: taskId, 'completition.user': Meteor.userId()}).count() > 0)
        {

            Tasks.update({_id: taskId, 'completition.user': Meteor.userId()} , {$set : {'completition.$.completition' : values}})
        }
        else
        {

            Tasks.update({_id: taskId} , {$push : {'completition' : {'completition' : values , 'user' :Meteor.userId()} }})
        }



    }
});