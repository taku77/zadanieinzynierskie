/**
 * Created by tomek on 21.11.15.
 */
T9n.setLanguage('pl');
if(Meteor.isClient){

    Template.registerHelper('formatDate', function(date){
        return moment(date).fromNow();
    });

    Template.registerHelper('slicer', function(text, max){
        var ending = text.length < max ?'' : '...';
        return text.slice(0, max)+ending ;
    });


    Template.registerHelper('checkAuthor', function(id){
        var list = Posts.find({$and : [{_id: id, author : Meteor.userId()}]});
        return list.count();
    });


    Template.registerHelper('userInitials', function(userID){
        var user = Meteor.users.find({_id: userID});


        var usermane= '';
        user.forEach(
            function(user){
               usermane = user.username;
            }
        );


        return usermane;
    });

}
