/**
 * Created by tomek on 21.11.15.
 */
FlowRouter.route('/',{
    name: 'homePage',
    triggersEnter: function(){
        if(Meteor.user())
        {

            return FlowRouter.go('/user')
        }
        else
        {

            return FlowRouter.go('/login');
        }
    }
});
FlowRouter.route('/login',{
    name: 'login',
    action: function(){
        BlazeLayout.render('loginpage')
    }
});


FlowRouter.route('/register',{
    action: function(){
        BlazeLayout.render('register')
    }
});




var userGroup = FlowRouter.group({
    triggersEnter: [function(){

    }],
    prefix: '/user'
});
var adminUserGroup = FlowRouter.group({
    prefix: '/admin'
});




userGroup.route('/',{
    name: 'postList',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'postlist'})
    }
});


userGroup.route('/show/post/:title',{
    name: 'showPost',
    action: function(params){
        console.log(params);
        BlazeLayout.render('bodyMain', {content: 'posts', param : params})
    }
});

userGroup.route('/editPost/:title',{
    name: 'editPost',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'editPost'})
    }
});
userGroup.route('/addPost',{
    name: 'addPost',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'addpost'})
    }
});


userGroup.route('/yourTasks',{
    name: 'yourTasks',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'yourtasks'})
    }
})

userGroup.route('/allFiles', {
    name: 'allFiles',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'allFiles'})
    }
})








/*Project*/

userGroup.route('/addProject',{
    name: 'addProject',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'addProject'})
    }
})


userGroup.route('/show/project/:id',{
    name: 'showProject',
    action: function(){
        BlazeLayout.render('bodyMain', {content: 'showproject'})
    }
})




/**Profile */
userGroup.route('/profile/:id',
    {
        name: 'showProfile',
        action: function(){
            BlazeLayout.render('bodyMain', {content: 'userProfile'})
        }
    })



var fail = function(response) {
    response.statusCode = 404;
    response.end();
};


FlowRouter.route('/download/file/:id',{
    name: 'downloadFile',
    action: function(){
        dataFile(FlowRouter.getParam('id'));
    }
});
var dataFile = function(id) {
    // TODO write a function to translate the id into a file path
    var file = '/home/tomek/Proejkty/meteor/zadanie/public/upload/user/Fotolia_86940946_XL.jpg';

    // Attempt to read the file size
    var stat = null;
    try {
        stat = fs.statSync(file);
    } catch (_error) {
        return fail(this.response);
    }




    // The hard-coded attachment filename
    var attachmentFilename = 'image.jpg';

    // Set the headers
    this.response.writeHead(200, {
        'Content-Type': 'image/jpg',
        'Content-Disposition': 'attachment; filename=' + attachmentFilename,
        'Content-Length': stat.size
    });

    // Pipe the file contents to the response
    fs.createReadStream(file).pipe(this.response);
};