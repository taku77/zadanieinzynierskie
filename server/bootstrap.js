/**
 * Created by tomek on 21.11.15.
 */
Meteor.startup(function () {


    var fs = Npm.require('fs');

    if (Posts.find().count() === 0) {

        var titles = [ "lorem" , "ipsum" , "other one", "final"];
        var desc = ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentesque ipsum tellus, a vehicula turpis euismod id. Curabitur pellentesque nibh vel gravida varius. Maecenas non blandit enim. Proin pretium turpis quis libero feugiat, eu pellentesque neque rutrum. Donec dictum feugiat ligula eu eleifend. Vestibulum pulvinar lacus sed mi pharetra, a tempus nibh fringilla. Ut condimentum erat at lacinia imperdiet. Mauris sed ultrices orci. Quisque euismod sodales ipsum vitae ultrices. ',
        'Nullam vitae sem vehicula eros feugiat lacinia. Ut velit leo, malesuada vehicula diam non, ultricies tincidunt magna. Nulla at magna nec ligula fringilla facilisis porttitor at ipsum. Ut ut malesuada turpis. Quisque tortor ante, accumsan sed suscipit id, sagittis consectetur est. Proin ut purus in elit ultricies vestibulum eu quis justo. Cras hendrerit, velit ut tincidunt porttitor, velit lectus ultricies mi, ut volutpat lorem erat id elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent at risus porta, malesuada magna nec, gravida libero. Sed semper sollicitudin ipsum in mollis.'
        ];
            _.each(titles, function (title) {
                Posts.insert({
                    title: title,
                    text: desc[Math.floor(Math.random() * desc.length)],
                    createdAt: new Date()
                });
            });
    }
    UploadServer.init({
        tmpDir : process.env.PWD + '/public/upload/tmp',
        uploadDir: process.env.PWD + '/public/upload',
        checkCreateDirectories: true,
        getDirectory: function(fileInfo, formData) {
            // create a sub-directory in the uploadDir based on the content type (e.g. 'images')

            return formData.dirProject+'/'+fileInfo.name.split('.')[0];
        },
        finished: function(fileInfo, formFields) {
            // perform a disk operation
        },
        cacheTime: 100,
        mimeTypes: {
            "xml": "application/xml",
            "vcf": "text/x-vcard"
        }
    })



});


if(Meteor.isServer){
    Meteor.publish('userData', function(){
        return Meteor.users.find({
            _id: this._id
        }, {fields: {'nick' : 1, 'address' : 1}})
    })
}



