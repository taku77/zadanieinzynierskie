/**
 * Created by tomek on 21.11.15.
 */

Template.latestPost.events ({
    'change .message-menu-inid' : function(){
        $('.message-menu-inid').effect('shake')
    }
});


Template.latestPost.helpers({
    posts: function(){
        var data = new Date();
        var yesterday = new Date(data);
        yesterday.setDate(data.getDate()-1);
        return Posts.find({createdAt: { $gt: yesterday}},{sort : {createdAt: -1}});
    },

    counter: function(){
        var data = new Date();
        var yesterday = new Date(data);
        yesterday.setDate(data.getDate()-1);
        return Posts.find({createdAt: { $gt: yesterday}},{sort : {createdAt: -1}}).count();
    }


});