/**
 * Created by tomek on 21.11.15.
 */




Template.latestTask.helpers({
    taski: function(){
        var data = new Date();
        var yesterday = new Date(data);
        yesterday.setDate(data.getDate()-1);

        return Tasks.find({$and : [{createdAt: { $lt: yesterday}},{forUsers: Meteor.userId()}]}, {sort: {createAt: 1}});
    },

    counter: function(){
        var data = new Date();
        var yesterday = new Date(data);
        yesterday.setDate(data.getDate()-1);
        return Tasks.find({$and : [{createdAt: { $lt: yesterday}},{forUsers: Meteor.userId()}]}, {sort: {createAt: 1}}).count();
    }


});