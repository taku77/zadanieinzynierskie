
//DEFAULTS
var EDITING = 'editingValue';
var EDITING_TITLE = 'editingTitle';


Session.setDefault(EDITING, false);
Session.setDefault(EDITING_TITLE, false);



//FUNCTIONS
var saveProjectText = function (project, template) {
    Session.set(EDITING,false);
    Project.update(project._id, {$set: {text: template.$('[name=body]').val()}});
};

var saveProjectTitle = function(project, template){
    Session.set(EDITING_TITLE, false);
    Project.update(project._id, {$set : {title: template.$('[name=title-text]').val()}})
};


Template.project.events({
    'click .project-text' : function(){
        if(Meteor.userId() == this.author)
            Session.set(EDITING, true);

    },
    'blur .project-text' : function(event, template){

        saveProjectText(this, template);
    },
    'click .project-title' : function(){
        if(Meteor.userId() == this.author)
            Session.set(EDITING_TITLE, true);

    },
    'blur .project-title' : function(event, template){
        saveProjectTitle(this, template);
    }

});



Template.project.helpers({
    editingKey: function(){
        return Session.get(EDITING);
    },
    editinTitle: function(){
        return Session.get(EDITING_TITLE);
    },
    usersFind: function(){
        if(Meteor.userId() == this.author)
        {
            var forUsers = Project.findOne(FlowRouter.getParam('id'));
            var userFinded = Meteor.users.find({_id: {$nin : forUsers.forUsers}});

            if(userFinded.count() > 0)
                return userFinded;

        }


        return false;
    }
});




//SHOWPROJECT
Template.showproject.helpers({
    testParam: function(){
        return Files.findOne({_id: "QfGgjp3pEQvAwok3j"});
    },
    projectDesc: function(){
        return Project.find({_id: FlowRouter.getParam('id')})
    }
});



Template.showproject.events({
    'click .info-box-right' : function(){
        var projectId = Project.findOne({_id : FlowRouter.getParam('id'), author: Meteor.userId() } );

        if(projectId)
            Meteor.call('removeDefinedUser', projectId, this._id );



    },

    'click .add-to-project' : function(e){


        e.preventDefault();
        var userSelected = $('.user-project').val();
        var projectId = Project.findOne({_id : FlowRouter.getParam('id'), author: Meteor.userId() } );
        if(projectId)
                Meteor.call('addUserToProject', projectId, userSelected)

    }
});