Template.addProject.onRendered( function() {
    $( ".add-project" ).validate(
        {
            messages: {
                text:{
                    required: 'To pole jest wymagane!'
                },
                title:{
                    required: 'To pole jest wymagane!'
                },
                dateend:{
                    required: 'To pole jest wymagane!'
                }

            }
        }
    );
});

Template.addProject.helpers({
    users: function(){
        return Meteor.users.find({});
    }
})


Template.addProject.events({
    'submit .add-project': function(e){
        e.preventDefault();

        var title = e.target.title.value;
        var text  = e.target.text.value;
        var date = new Date(e.target.dateend.value);
        var users = [] ;

        $('select[name="users"] :selected').each(function(i, selected){
            users[i] = $(selected).val()
        });



        Project.insert({
            title: title,
            text: text,
            dueDate: date,
            forUsers: users,

            author: Meteor.userId(),
            createdAt: new Date()
        });


        return FlowRouter.go('/user')

    }
})