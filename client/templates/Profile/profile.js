var EDITING_FILE = 'editingFile';
var AUTHOR_OF_THIS = 'ownerOfFiles';


Session.setDefault(AUTHOR_OF_THIS, false);
Template.userProfile.helpers({
    isUser: function(){
        return FlowRouter.getParam('id') == Meteor.userId()


    },
    userProfiler : function(){

        return Meteor.users.findOne({_id : FlowRouter.getParam('id')})
    },
    users: function(){
        return Meteor.users.find({_id : {$ne : Meteor.userId()}});
    },
    userOwnFiles: function(){

        if(FlowRouter.getParam('id') == Meteor.userId())
        {
            Session.set(AUTHOR_OF_THIS, true)
            return Files.find({userID : FlowRouter.getParam('id')});
        }
        else
        {
            Session.set(AUTHOR_OF_THIS, false)
            return Files.find({$and:[{userID : FlowRouter.getParam('id')}, {usersAllowed : Meteor.userId()}] })
        }


    },
    myCallbacks : function(){
        return {
            finished: function(index, fileInfo,  context){

                var users = [];
                $('select[name="usersFileAllowed"] :selected').each(function(i, selected){
                    users[i] = $(selected).val()
                });
                fileInfo['userID'] = Meteor.userId();
                fileInfo['usersAllowed'] = users;
                fileInfo['createdAt'] = new Date();
                Files.insert(fileInfo);
            }
        }
    },
    customFormData: function(){
        return {
            dirProject: 'users/'+Meteor.userId()
        }
    }
});
