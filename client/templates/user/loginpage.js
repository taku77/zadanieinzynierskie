/**
 * Created by tomek on 21.11.15.
 */


Template.loginpage.events({
    'submit .login-form': function(e){
        T9n.setLanguage("pl")
        e.preventDefault();
        $('.help-block').html('');
        var login = e.target.user.value,
            password = e.target.password.value;

        Meteor.loginWithPassword({username: login}, password, function(err){
            if(err){

                $('.help-block').html(err.reason);
            }
            else{
                return FlowRouter.go('/user')
            }


        })


    }
});


Template.register.events({
    'submit .register-form' : function(e){
        e.preventDefault();
        T9n.setLanguage("pl")
        $('.help-block').html('');
        var login = e.target.user.value,
            password = e.target.password.value;


        Accounts.createUser({username: login, password: password, createdAt: new Date()}, function(err){
            if(err)
            {
                $('.help-block').html(err.reason);
            }
            else
            {
                FlowRouter.go('/user')
            }
        })
    }
});