var pictures = ['jpg', 'jpeg', 'png', 'bmp'];
var docs = ['doc', 'odt' , 'docx', 'txt'];
var zip = ['zip' , 'tar', 'rar'];
var AUTHOR_OF_THIS = 'ownerOfFiles';
Template.userFiles.helpers({
    author : function(){
        return Session.get(AUTHOR_OF_THIS);
    },
    users: function(){
        return Meteor.users.find({_id : {$ne : Meteor.userId()}});
    },
    extension : function(){
        var ext = this.name.split(".")[1].toLowerCase();

        var returner = [];
        pictures.forEach(function(pic){
            if(pic == ext)
                returner = {
                    'fa' : 'fa-picture-o',
                    'bg' : 'bg-aqua'
                }
        });

        docs.forEach(function(doki){
            if(doki == ext)
                returner = {
                    'fa' : 'fa-file-text',
                    'bg' : 'bg-green'
                }
        });

        zip.forEach(function(ziper){
            if(ziper == ext)
                returner = {
                    'fa' : 'fa-file-archive-o',
                    'bg' : 'bg-red'
                }
        });


        if(returner['fa'])
            return returner
        else
            return {
                'fa' : 'fa-globe',
                'bg' : 'bg-blue'
            }
    }
});

Template.userFiles.events({
   'click .saveUsersAllowed' : function(){
       var users = [];
       $('select[name="usersFile"] :selected').each(function(i, selected){
           users[i] = $(selected).val()
       });

       Files.update({_id : this._id},{$set :{usersAllowed : users}});
   }
});
Template.usersEdition.helpers({
    isChckeded : function(){
        var id = this._id;
        var returner = '';
        Template.parentData().usersAllowed.forEach(function(param){
                if(id == param)
                    returner = 'selected';

        });

        return  returner;
    }
})
