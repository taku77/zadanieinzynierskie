


Template.latestFiles.helpers({
    files : function(){
        var data = new Date();
        var daysbefore = new Date(data);
        daysbefore.setDate(data.getDate()-10);
        return Files.find({$and : [{createdAt: { $gt: daysbefore}},{usersAllowed : Meteor.userId()}]},{sort : {createdAt : 1}})
    }
})