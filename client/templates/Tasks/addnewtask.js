Template.addnewtask.onRendered( function(){
    $( ".add-task" ).validate(
        {
            messages: {
                text:{
                    required: 'To pole jest wymagane!'
                },
                title:{
                    required: 'To pole jest wymagane!'
                },
                dateend:{
                    required: 'To pole jest wymagane!'
                }

            }
        }
    );
})

Template.addnewtask.helpers({
    users: function(){
        return Meteor.users.find({});
    }
});


Template.addnewtask.events({
    'submit .add-task': function(e){
        e.preventDefault();

        var title = e.target.title.value;
        var text  = e.target.text.value;
        var date = new Date(e.target.dateend.value);
        var users = [] ;

        $('select[name="users"] :selected').each(function(i, selected){
            users[i] = $(selected).val()
        })



        Tasks.insert({
            title: title,
            text: text,
            dueDate: date,
            forUsers: users,
            project: FlowRouter.getParam('id'),
            author: Meteor.userId(),
            createdAt: new Date()
        })

    }
})
