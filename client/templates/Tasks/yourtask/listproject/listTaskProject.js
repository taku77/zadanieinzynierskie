/**
 * Created by tomek on 18.02.16.
 */
Template.listTaskProject.helpers({
    tasks : function(){
        return Tasks.find({$and : [{project: this._id},{forUsers: Meteor.userId()}]}, {sort: {dueDate: 1}})
        //return Tasks.find({project: this._id}, {sort: {dueDate: 1}})
    },


});


Template.listOneTask.events({
    'submit .task-form': function(e, template){
        e.preventDefault();


        Meteor.call('updateTask', this._id, e.target.taskValue.value)

        template.$('.info-box-object').fadeIn().html('<span class="label label-success">Zauktalizowano pomyślnie</span>')
        $('.info-box-object').delay(1200).fadeOut()
    }
})
Template.listOneTask.helpers({
    completitionPercent : function(){
        var completition = this.completition;
        var userPercent = 0;
        if(this.completition)
        {
            completition.forEach(function(v){
                if(v.user == Meteor.userId())
                {
                    userPercent = v.completition
                }
            })


        }
        return userPercent ;
    },
    isCompleted : function(){
        var completition = this.completition;
        var userPercent = 0;
        if(this.completition)
        {
            completition.forEach(function(v){
                if(v.user == Meteor.userId())
                {
                    userPercent = v.completition
                }
            })


        }



        return userPercent == '100' ;
    }
})