/**
 * Created by tomek on 18.02.16.
 */
if(Meteor.isClient){
    Template.yourtasks.rendered = function(){
        $('.slider').slider();
    }

}

Template.yourtasks.helpers({
    project: function(){
        return Project.find({$or : [{forUsers: Meteor.userId()}, {author : Meteor.userId() }]})
    }
});