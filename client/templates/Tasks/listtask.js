
var lastDate = null;
var setNewDate = false;
Template.listtask.helpers({
    taski: function(){
        return Tasks.find({$and : [{project: FlowRouter.getParam('id')},{$or : [{forUsers: Meteor.userId()}, {author : Meteor.userId() }] }]}, {sort: {dueDate: 1}})
    }


});


Template.onetask.onRendered(function(){
$('.time-line-animation').fadeIn('slow');
});
Template.onetask.helpers({

    checkNewDate : function(){
        setNewDate = false;

        if(!lastDate)
        {
            lastDate = this.dueDate;
        }

        if(lastDate && (this.dueDate.getTime() != lastDate.getTime()))
        {
            lastDate = this.dueDate;
            setNewDate = true;
        }


        return setNewDate;
    },
    timetoend: function(){

        if(this.dueDate > new Date())
            return 'bg-green'
        else
            return 'bg-red'

    }
});


