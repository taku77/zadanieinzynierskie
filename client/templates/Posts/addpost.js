/**
 * Created by tomek on 21.11.15.
 */


Template.addpost.events({
    "submit .new-post": function(e) {
        e.preventDefault();

        var title = e.target.title.value;
        var text = e.target.text.value;

        Posts.insert({
            title: title,
            text: text,
            createdAt: new Date(),
            author: Meteor.userId()
        });

        e.target.title.value = "";
        e.target.text.value = "";

        return FlowRouter.go('/');
    }
})
