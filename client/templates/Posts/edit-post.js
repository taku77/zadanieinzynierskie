Template.editPost.helpers({
    posts: function(){
        var title = FlowRouter.getParam('title');


        return Posts.find({_id: title})
    }
});
Template.editForm.events({
    'submit .edit-post' : function(e){
        e.preventDefault();


        var title = e.target.title.value;
        var text = e.target.text.value;


        Posts.update(
            {_id : this._id},
            {
                $set : {
                    title: title,
                    text: text
                }

            }
        );


        FlowRouter.go(FlowRouter.path('showPost', {title: this._id}))

    }
})