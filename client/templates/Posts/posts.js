/**
 * Created by tomek on 21.11.15.
 */

Template.posts.helpers(
    {
       postRender: function(){
           var postTitle = FlowRouter.getParam('title');
           return Posts.find({_id: postTitle});
       }

    }

);
