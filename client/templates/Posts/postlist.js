/**
 * Created by tomek on 21.11.15.
 */



Template.postlist.helpers({
    posts: function(){
        return Posts.find({}, {sort: {createdAt: -1}});
    }
});